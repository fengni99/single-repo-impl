package com.discovery;
/**
 * Copyright (c) 2015 Baozun All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Baozun.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Baozun.
 *
 * BAOZUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. BAOZUN SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */


import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.discovery.darchrow.single.mapper.UserMapper;
import com.discovery.darchrow.tools.jsonlib.JsonUtil;
public class UserDaoTest extends BaseTest{
	
	private Logger log =LoggerFactory.getLogger(UserDaoTest.class);
	
	@Autowired
	private UserMapper userMapper;
	
	@Test
	public void testFindUserById(){
		
		log.debug("test-------------:{}",JsonUtil.format(userMapper.selectUserByID(1)));
	} 
}
