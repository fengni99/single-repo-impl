/**
 * Copyright (c) 2016 Baozun All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Baozun.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Baozun.
 *
 * BAOZUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. BAOZUN SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.discovery.darchrow.single.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.discovery.darchrow.single.api.DemoUserService;
import com.discovery.darchrow.single.mapper.UserMapper;
import com.discovery.darchrow.single.model.User;


@Service
@Transactional(value = "store", rollbackFor = Exception.class)  
public class DemoUserServiceImpl implements DemoUserService{
	
	private static final Logger log = LoggerFactory.getLogger(DemoUserServiceImpl.class);
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public User findById(int id) {
		log.debug("query data from DB !");
		return userMapper.selectUserByID(id);
	}

}
