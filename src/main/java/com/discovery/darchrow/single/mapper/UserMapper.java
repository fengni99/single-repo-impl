package com.discovery.darchrow.single.mapper;

import java.util.List;

import com.discovery.darchrow.single.model.User;


public interface UserMapper {
	
	public User selectUserByID(int id);
	
	public List<User> selectUsers(String userName);
}